package com.zdzieblo.movieshowcase.manager

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager

class SharedPreferenceManager(
    context: Context
) {

    private val preferences: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    fun setTutorialCompleted() {
        preferences.edit().apply {
            putBoolean(KEY_TUTORIAL_COMPLETED, true)
        }.apply()
    }

    fun isTutorialCompleted(): Boolean {
        return preferences.getBoolean(KEY_TUTORIAL_COMPLETED, false)
    }

    companion object {
        const val KEY_TUTORIAL_COMPLETED = "tut_completed"
    }
}