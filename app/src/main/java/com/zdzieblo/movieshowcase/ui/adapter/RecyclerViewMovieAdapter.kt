package com.zdzieblo.movieshowcase.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zdzieblo.movieshowcase.R
import com.zdzieblo.movieshowcase.model.MovieItem
import com.zdzieblo.movieshowcase.service.FavoriteMoviesDao

class RecyclerViewAdapter(
    private val list: MutableList<MovieItem?>,
    private val favoriteMoviesDao: FavoriteMoviesDao
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_ITEM = 0
        const val TYPE_LOADING = 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (list[position] == null) TYPE_LOADING else TYPE_ITEM
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)

        if (viewType == TYPE_ITEM) {
            val view: View = layoutInflater.inflate(R.layout.movie_row, parent, false)
            return RecyclerViewMovieHolder(view, favoriteMoviesDao)
        } else {
            val view: View = layoutInflater.inflate(R.layout.item_loading, parent, false)
            return RecyclerViewSimpleHolder<Any>(view)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item: MovieItem? = list[position]

        if (holder is RecyclerViewMovieHolder) {
            item?.let { holder.bind(it) }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    /**
     * Add progress bar at the bottom of recycler view by adding null as last item
     */
    fun setLoading(loading: Boolean) {
        if (loading) {
            list.add(null)
            notifyItemInserted(list.size - 1)
        } else if (list[list.size - 1] == null) {
            list.removeAt(list.size - 1)
            notifyDataSetChanged()
        }
    }

}