package com.zdzieblo.movieshowcase.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

open class RecyclerViewSimpleHolder<T>(itemView: View) :
    RecyclerView.ViewHolder(itemView)