package com.zdzieblo.movieshowcase.ui.activity

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.github.salomonbrys.kodein.KodeinInjected
import com.github.salomonbrys.kodein.KodeinInjector
import com.github.salomonbrys.kodein.instance
import com.zdzieblo.movieshowcase.R
import com.zdzieblo.movieshowcase.db.AppDatabase
import com.zdzieblo.movieshowcase.kodeinGlobal
import com.zdzieblo.movieshowcase.model.FavoriteMovie
import com.zdzieblo.movieshowcase.model.MovieDetail
import com.zdzieblo.movieshowcase.service.MoviesService
import kotlinx.android.synthetic.main.activity_movie_detail.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class MovieDetailActivity : AppCompatActivity(), KodeinInjected {

    companion object {
        const val BUNDLE_IMDB_ID: String = "imdbId"
    }

    override val injector: KodeinInjector = KodeinInjector()

    private val retrofit: Retrofit by instance()
    private val db: AppDatabase by instance()

    private val moviesService: MoviesService

    init {
        injector.inject(kodeinGlobal())
        moviesService = retrofit.create(MoviesService::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        val imdbId: String = checkNotNull(intent.getStringExtra(BUNDLE_IMDB_ID))

        GlobalScope.async {
            detailFavoriteCheckBox.isChecked = db.favoriteMoviesDao().findById(imdbId) != null
        }
        detailFavoriteCheckBox.setOnClickListener {
            if (detailFavoriteCheckBox.isChecked) {
                runBlocking {
                    db.favoriteMoviesDao().insert(FavoriteMovie(imdbId))
                }
            } else {
                runBlocking {
                    db.favoriteMoviesDao().delete(FavoriteMovie(imdbId))
                }
            }
        }

        loadMovieDetail(imdbId)
    }

    private fun loadMovieDetail(imdbId: String) {

        val movieDetailCall = moviesService.getMovieDetail(imdbId)
        movieDetailCall.enqueue(object : Callback<MovieDetail> {
            override fun onFailure(call: Call<MovieDetail>, t: Throwable) {
                Toast.makeText(
                    this@MovieDetailActivity,
                    t.message,
                    Toast.LENGTH_LONG
                ).show()
            }

            override fun onResponse(call: Call<MovieDetail>, response: Response<MovieDetail>) {
                val detail: MovieDetail? = response.body()

                supportActionBar?.title = detail?.title

                detailTitle.text = detail?.title
                detailType.text = getString(R.string.detail_type, detail?.type, detail?.year)
                detailRuntime.text = detail?.runtime
                detailPlot.text = detail?.plot
                detailActors.text = getString(R.string.detail_actors, detail?.actors)

                val circularProgressDrawable = CircularProgressDrawable(this@MovieDetailActivity)
                circularProgressDrawable.strokeWidth = 5f
                circularProgressDrawable.centerRadius = 30f
                circularProgressDrawable.start()

                Glide.with(this@MovieDetailActivity)
                    .load(detail?.posterLink)
                    .placeholder(circularProgressDrawable)
                    .error(android.R.drawable.ic_menu_close_clear_cancel)
                    .into(detailPosterImage)
            }

        })
    }
}