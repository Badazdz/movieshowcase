package com.zdzieblo.movieshowcase.ui.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.salomonbrys.kodein.KodeinInjected
import com.github.salomonbrys.kodein.KodeinInjector
import com.github.salomonbrys.kodein.instance
import com.zdzieblo.movieshowcase.R
import com.zdzieblo.movieshowcase.db.AppDatabase
import com.zdzieblo.movieshowcase.kodeinGlobal
import com.zdzieblo.movieshowcase.manager.SharedPreferenceManager
import com.zdzieblo.movieshowcase.model.MovieItem
import com.zdzieblo.movieshowcase.model.Search
import com.zdzieblo.movieshowcase.service.MoviesService
import com.zdzieblo.movieshowcase.ui.adapter.RecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt


class MainActivity : AppCompatActivity(), KodeinInjected {

    companion object {
        const val VISIBLE_THRESHOLD = 5
    }

    override val injector: KodeinInjector = KodeinInjector()

    private val retrofit: Retrofit by instance()
    private val db: AppDatabase by instance()

    private val prefsManager: SharedPreferenceManager by instance()

    private val moviesList: MutableList<MovieItem?> = ArrayList()
    private val moviesAdapter: RecyclerViewAdapter

    private var lastSearchSize: Int = 0
    private var lastSearchPage: Int = 0
    private var lastSearchQuery: String = ""
    private var loading: Boolean = false

    private val moviesService: MoviesService

    init {
        injector.inject(kodeinGlobal())
        moviesService = retrofit.create(MoviesService::class.java)
        moviesAdapter = RecyclerViewAdapter(
            moviesList,
            db.favoriteMoviesDao()
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        // init recycler view
        val linearLayoutManager = LinearLayoutManager(this@MainActivity)
        moviesRecyclerView.adapter = moviesAdapter
        moviesRecyclerView.layoutManager = linearLayoutManager
        moviesRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                // automatically load next items when user is close to the bottom

                val visibleItemCount = linearLayoutManager.childCount
                val totalItemCount = linearLayoutManager.itemCount
                val pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition()

                val shouldLoad =
                    (visibleItemCount + pastVisibleItems + VISIBLE_THRESHOLD) >= totalItemCount

                if (!loading && shouldLoad && moviesList.size < lastSearchSize && lastSearchQuery.isNotEmpty()) {
                    setLoading(true, recyclerView)
                    searchNextPage(lastSearchQuery, moviesService)
                }
            }
        })
    }

    /**
     * Sets [loading] property and adds progress bar to recycler view.
     * When [recyclerView] is set, loading in the adapter will be called by post
     */
    private fun setLoading(loading: Boolean, recyclerView: RecyclerView? = null) {
        this@MainActivity.loading = loading
        if (recyclerView == null) {
            moviesAdapter.setLoading(loading)
        } else {
            // do not set loading immediately because it adds items to underlying list of values
            // which will cause errors when calling from onScrolled
            recyclerView.post {
                moviesAdapter.setLoading(loading)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        val moviesService: MoviesService = retrofit.create(MoviesService::class.java)

        // init search button
        val actionSearch = menu.findItem(R.id.action_search)
        val searchView: SearchView = actionSearch.actionView as SearchView
        searchView.queryHint = "Ex. friends"
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                searchView.clearFocus()

                if (loading) {
                    return true
                }

                // clear values for new query
                lastSearchSize = 0
                lastSearchPage = 0
                lastSearchQuery = checkNotNull(query)
                moviesList.clear()
                moviesAdapter.notifyDataSetChanged()

                if (query.isNotEmpty()) {
                    setLoading(true)
                    searchNextPage(query, moviesService)
                    return false
                }

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })

        // highlight search icon for the first only
        if (!prefsManager.isTutorialCompleted()) {
            MaterialTapTargetPrompt.Builder(this@MainActivity)
                .setTarget(searchView)
                .setPrimaryText("Try your first search")
                .setSecondaryText("Tap the magnifying glass to start searching")
                .setPromptStateChangeListener { prompt, state ->
                    if (state == MaterialTapTargetPrompt.STATE_DISMISSED
                        || state == MaterialTapTargetPrompt.STATE_FINISHED) {
                        prefsManager.setTutorialCompleted()
                    }
                }
                .show()
        }

        return true
    }

    private fun searchNextPage(
        query: String,
        moviesService: MoviesService
    ) {
        val searchMovies: Call<Search> = moviesService.searchMovies(query, ++lastSearchPage)

        searchMovies.enqueue(object : Callback<Search> {
            override fun onFailure(call: Call<Search>, t: Throwable) {
                setLoading(false)
                Toast.makeText(
                    this@MainActivity,
                    t.message,
                    Toast.LENGTH_LONG
                ).show()
            }

            override fun onResponse(
                call: Call<Search>,
                response: Response<Search>
            ) {
                setLoading(false)

                val value: Search? = response.body()

                if (value?.error != null) {
                    Toast.makeText(
                        this@MainActivity,
                        value.error,
                        Toast.LENGTH_LONG
                    ).show()
                } else if (value?.totalResults != 0) {

                    lastSearchSize = value?.totalResults ?: 0
                    value?.searchList?.let { moviesList.addAll(it) }
                    moviesAdapter.notifyDataSetChanged()
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_search -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
