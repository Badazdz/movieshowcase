package com.zdzieblo.movieshowcase.ui.adapter

import android.content.Intent
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.zdzieblo.movieshowcase.R
import com.zdzieblo.movieshowcase.model.FavoriteMovie
import com.zdzieblo.movieshowcase.model.MovieItem
import com.zdzieblo.movieshowcase.service.FavoriteMoviesDao
import com.zdzieblo.movieshowcase.ui.activity.MovieDetailActivity
import com.zdzieblo.movieshowcase.ui.activity.MovieDetailActivity.Companion.BUNDLE_IMDB_ID
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking

/**
 * Describes a movie item and contains convenience [bind] function for setting all properties
 */
class RecyclerViewMovieHolder(itemView: View, private val favoriteMoviesDao: FavoriteMoviesDao) :
    RecyclerViewSimpleHolder<MovieItem>(itemView) {

    private val txtTitle: TextView = itemView.findViewById(R.id.title)
    private val txtType: TextView = itemView.findViewById(R.id.type)
    private val coverImage: ImageView = itemView.findViewById(R.id.posterImage)
    private val favoriteCheckBox: CheckBox = itemView.findViewById(R.id.favoriteCheckBox)

    fun bind(item: MovieItem) {

        val context = itemView.context

        itemView.setOnClickListener {

            val intent = Intent(context, MovieDetailActivity::class.java).apply {
                putExtra(BUNDLE_IMDB_ID, item.imdbId)
            }

            context.startActivity(intent)
        }

        txtTitle.text = item.title
        txtType.text = context.getString(R.string.detail_type, item.type, item.year)

        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        Glide.with(context)
            .load(item.posterLink)
            .centerCrop()
            .placeholder(circularProgressDrawable)
            .error(android.R.drawable.ic_menu_close_clear_cancel)
            .into(coverImage)

        // init favorite button
        GlobalScope.async {
            favoriteCheckBox.isChecked = favoriteMoviesDao.findById(item.imdbId) != null
        }
        favoriteCheckBox.setOnClickListener {
            if (favoriteCheckBox.isChecked) {
                runBlocking {
                    favoriteMoviesDao.insert(FavoriteMovie(item.imdbId))
                }
            } else {
                runBlocking {
                    favoriteMoviesDao.delete(FavoriteMovie(item.imdbId))
                }
            }
        }
    }
}