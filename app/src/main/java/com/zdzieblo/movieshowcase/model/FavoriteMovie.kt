package com.zdzieblo.movieshowcase.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class FavoriteMovie (
    @PrimaryKey val imdbId: String
)