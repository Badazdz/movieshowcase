package com.zdzieblo.movieshowcase.model

import com.google.gson.annotations.SerializedName

class MovieItem(
    @field:SerializedName("Title") var title: String,
    @field:SerializedName("Year") var year: String,
    @field:SerializedName("imdbID") var imdbId: String,
    @field:SerializedName("Type") var type: String,
    @field:SerializedName("Poster") var posterLink: String
)
