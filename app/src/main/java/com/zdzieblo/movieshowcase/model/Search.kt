package com.zdzieblo.movieshowcase.model

import com.google.gson.annotations.SerializedName

class Search (
    @field:SerializedName("Search") var searchList: List<MovieItem>?,
    @field:SerializedName("totalResults") var totalResults: Int?,
    @field:SerializedName("Error") var error: String?
)