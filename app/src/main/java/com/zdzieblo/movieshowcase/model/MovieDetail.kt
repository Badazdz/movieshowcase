package com.zdzieblo.movieshowcase.model

import com.google.gson.annotations.SerializedName

class MovieDetail(
    @field:SerializedName("Title") var title: String,
    @field:SerializedName("Year") var year: String,
    @field:SerializedName("Runtime") var runtime: String,
    @field:SerializedName("Genre") var genre: String,
    @field:SerializedName("Director") var director: String,
    @field:SerializedName("Writer") var writer: String,
    @field:SerializedName("Actors") var actors: String,
    @field:SerializedName("Plot") var plot: String,
    @field:SerializedName("Country") var country: String,
    @field:SerializedName("Type") var type: String,
    @field:SerializedName("imdbID") var imdbId: String,
    @field:SerializedName("imdbRating") var imdbRating: String,
    @field:SerializedName("Poster") var posterLink: String
)