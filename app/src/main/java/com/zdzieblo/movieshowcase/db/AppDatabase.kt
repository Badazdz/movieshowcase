package com.zdzieblo.movieshowcase.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.zdzieblo.movieshowcase.model.FavoriteMovie
import com.zdzieblo.movieshowcase.service.FavoriteMoviesDao

@Database(entities = [FavoriteMovie::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun favoriteMoviesDao(): FavoriteMoviesDao
}