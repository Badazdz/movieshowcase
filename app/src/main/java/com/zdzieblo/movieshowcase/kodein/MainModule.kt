package com.zdzieblo.movieshowcase.kodein

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton
import com.zdzieblo.movieshowcase.R
import com.zdzieblo.movieshowcase.db.AppDatabase
import com.zdzieblo.movieshowcase.manager.SharedPreferenceManager
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val mainModule = Kodein.Module {
    bind<Retrofit>() with singleton {

        // create retrofit instance with interceptor that adds apikey to each request
        Retrofit.Builder()
            .client(OkHttpClient().newBuilder().addInterceptor(object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {
                    val original: Request = chain.request()
                    val originalHttpUrl: HttpUrl = original.url()

                    val apiKey: String = instance<Context>().getString(R.string.apikey)

                    val url = originalHttpUrl.newBuilder()
                        .addQueryParameter("apikey", apiKey)
                        .build()

                    val requestBuilder: Request.Builder = original.newBuilder()
                        .url(url)

                    val request: Request = requestBuilder.build()
                    return chain.proceed(request)
                }
            }).build())
            .baseUrl("http://www.omdbapi.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    bind() from singleton { SharedPreferenceManager(instance()) }

    bind() from singleton {
        Room.databaseBuilder(
            instance(),
            AppDatabase::class.java,
            "movie-showcase"
        ).build()
    }
}