package com.zdzieblo.movieshowcase.service

import com.zdzieblo.movieshowcase.model.MovieDetail
import com.zdzieblo.movieshowcase.model.Search
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface MoviesService {

    @GET(".")
    fun searchMovies(@Query("s") searchTerm: String, @Query("page") page: Int): Call<Search>

    @GET(".")
    fun getMovieDetail(@Query("i") imdbId: String): Call<MovieDetail>
}