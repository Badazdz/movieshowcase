package com.zdzieblo.movieshowcase.service

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.zdzieblo.movieshowcase.model.FavoriteMovie

@Dao
interface FavoriteMoviesDao {

    @Query("SELECT * FROM favoritemovie LIMIT 20 OFFSET :page * 20")
    suspend fun getAll(page: Int): List<FavoriteMovie>

    @Query("SELECT * FROM favoritemovie WHERE imdbId = :imdbId")
    suspend fun findById(imdbId: String): FavoriteMovie?

    @Insert
    suspend fun insert(favoriteMovie: FavoriteMovie)

    @Delete
    suspend fun delete(favoriteMovie: FavoriteMovie)
}