package com.zdzieblo.movieshowcase

import android.app.Application
import android.content.Context
import com.github.salomonbrys.kodein.*
import com.github.salomonbrys.kodein.android.autoAndroidModule
import com.zdzieblo.movieshowcase.kodein.mainModule

class MovieShowcaseApp : Application(), KodeinAware {

    override val kodein by Kodein.lazy {
        import(autoAndroidModule(this@MovieShowcaseApp))

        bind<Context>() with instance(this@MovieShowcaseApp)

        import(mainModule)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        private var instance: MovieShowcaseApp? = null

        fun get(): MovieShowcaseApp {
            return checkNotNull(instance)
        }
    }
}

fun kodeinGlobal() = MovieShowcaseApp.get().kodein